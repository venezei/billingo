# PartnerCustomBillingSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payment_method** | [**OneOfPartnerCustomBillingSettingsPaymentMethod**](OneOfPartnerCustomBillingSettingsPaymentMethod.md) |  | [optional] 
**document_form** | [**OneOfPartnerCustomBillingSettingsDocumentForm**](OneOfPartnerCustomBillingSettingsDocumentForm.md) |  | [optional] 
**due_days** | **int** |  | [optional] 
**document_currency** | [**OneOfPartnerCustomBillingSettingsDocumentCurrency**](OneOfPartnerCustomBillingSettingsDocumentCurrency.md) |  | [optional] 
**template_language_code** | [**OneOfPartnerCustomBillingSettingsTemplateLanguageCode**](OneOfPartnerCustomBillingSettingsTemplateLanguageCode.md) |  | [optional] 
**discount** | [**OneOfPartnerCustomBillingSettingsDiscount**](OneOfPartnerCustomBillingSettingsDiscount.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

